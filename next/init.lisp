(in-package :next)

(setf (get-default 'port 'path)
	  (format nil "~a/usr/bin/next-gtk-webkit"
			  (uiop:getenv "HOME")))
